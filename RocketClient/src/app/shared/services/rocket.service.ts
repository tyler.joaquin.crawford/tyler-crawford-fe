import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { RocketNode } from '../models/rocket-node';
import { environment } from "src/environments/environment"

@Injectable({
  providedIn: 'root'
})
export class RocketsService {
  relativeUrl: string = "rockets/nodes";

  constructor(private httpClient: HttpClient) { }

  public getRocketNodes(): Observable<RocketNode[]> {
    return this.httpClient.get<RocketNode[]>(`${environment.rocketsManagerApiUrl}/rockets/nodes`);
  }

  public getRocketNodesByPath(path: string): Observable<RocketNode> {
    return this.httpClient.get<RocketNode>(
      `${environment.rocketsManagerApiUrl}/rockets/nodes/path`,
      { params: { "path": path } });
  }
}
