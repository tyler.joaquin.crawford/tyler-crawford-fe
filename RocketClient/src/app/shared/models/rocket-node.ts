import { RocketTree } from "./rocket-tree";

export class RocketNode extends RocketTree<RocketNode> {
    name?: string;
    properties?: Record<string, number> = undefined;
    created?: Date;
    modified?: Date;
}