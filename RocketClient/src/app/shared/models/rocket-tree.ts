export class RocketTree<T>
{
    private _parentId?: string;
    private _children: T[] = [];

    id?: string;
    parent?: T;

    get parentId(): string | undefined {
        return this._parentId;
    }

    set parentId(parentId: string | undefined) {
        this._parentId = parentId;
    }

    public get children() {
        return this._children;
    }

    public set children(children: T[]) {
        let scope = this;
        children.forEach(child => {
            if ((child as RocketTree<T>).parent == null)
                Object.assign((scope as RocketTree<T>), (child as RocketTree<T>).parent);
        });
        this._children = children;
    }

    traverse(this: RocketTree<T>): RocketTree<T>[] {
        let nodes: RocketTree<T>[] = [];
        let queue = [this];

        while (queue.length > 0) {
            let current = queue.pop();
            nodes.push(current!);

            if (current!.children && current!.children.length > 0)
                current!.children.forEach(child =>
                    queue.push(child as RocketTree<T>));
        }

        return nodes;
    }
}