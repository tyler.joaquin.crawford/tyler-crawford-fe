import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'timeSince'
})
export class TimeSincePipe implements PipeTransform {

  transform(value: string): string {
    if (!value || value == "") return "N/A";
    let now: Date = new Date();
    let toTransform = new Date(value);

    // ref: https://sabe.io/blog/javascript-convert-milliseconds-seconds-minutes-hours
    let offset = now.getTime() - toTransform.getTime();
    const seconds = Math.floor((offset / 1000) % 60);
    const minutes = Math.floor((offset / 1000 / 60) % 60);
    const hours = Math.floor((offset / 1000 / 60 / 60) % 24);

    if (hours === 1) return `${hours} hour ago`;
    else if (hours > 0) return `${hours} hours ago`;
    else if (minutes === 1) return `${minutes} minute ago`;
    else if (minutes > 0) return `${minutes} minutes ago`;
    else if (seconds === 1) return `${seconds} second ago`;
    else if (seconds > 0) return `${seconds} seconds ago`;
    else return `just a moment ago`;
  }
}

@Pipe({ name: 'exponentialStrength' })
export class ExponentialStrengthPipe implements PipeTransform {
  transform(value: number, exponent = 1): number {
    return Math.pow(value, exponent);
  }
}
