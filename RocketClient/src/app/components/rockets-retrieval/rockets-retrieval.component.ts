import { Component } from '@angular/core';
import { catchError, debounceTime, EMPTY, filter, Subject, switchMap, takeUntil } from 'rxjs';
import { RocketNode } from 'src/app/shared/models/rocket-node';
import { RocketsService } from 'src/app/shared/services/rocket.service';

@Component({
  selector: 'app-rockets-retrieval',
  templateUrl: './rockets-retrieval.component.html',
  styleUrls: ['./rockets-retrieval.component.css']
})
export class RocketsRetrievalComponent {
  private _unsubscribe$ = new Subject<void>();
  private _path$ = new Subject<string>();
  private _pathRegex: RegExp = new RegExp("^(\\/([a-zA-Z\\d])+)+");

  rocketPathInput: string = "";
  pathKeyUp = new Subject<KeyboardEvent>();
  foundRocketNode?: RocketNode;

  constructor(private rocketsService: RocketsService) {
    this._path$
      .pipe(
        // Delay call so we can reduce/cancel calls to the API
        debounceTime(500),

        switchMap((path) => {
          // Try to fetch a rocket node by path
          return this.rocketsService
            .getRocketNodesByPath(path)
            .pipe(
              // Apparently switchmap unsubscribes if an error is returned
              filter((node) => {
                if (!node) throw new Error("Houston, we have a problem.");
                return true;
              }),
              // Return a valid (non-error) observable if error occurs
              catchError(err => EMPTY))
        }))
      .subscribe((node) => {
        // Assign the returned rocket node
        if (node) this.foundRocketNode = node;
      });
  }

  searchByPath(path: string) {
    // Ensure the path is a valid path
    !path || !this._pathRegex.test(path);
    this.foundRocketNode = undefined;
    this._path$.next(path);
  }

  deleteRocketNode(node: RocketNode) {
    console.info(`[${node.name}] Sending delete request to server!`);
  }

  ngOnDestroy(): void {
    this._unsubscribe$.next();
    this._unsubscribe$.unsubscribe();
  }
}
