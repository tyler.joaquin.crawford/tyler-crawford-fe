import { NestedTreeControl } from '@angular/cdk/tree';
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatTreeNestedDataSource } from '@angular/material/tree';
import { Subject, takeUntil } from 'rxjs';
import { RocketNode } from 'src/app/shared/models/rocket-node';
import { RocketConfirmDialogComponent } from '../rocket-confirm-dialog/rocket-confirm-dialog.component';

@Component({
  selector: 'app-rockets-display',
  templateUrl: './rockets-display.component.html',
  styleUrls: ['./rockets-display.component.css']
})
export class RocketsDisplayComponent {
  @Input() rocketNode?: RocketNode;
  @Output() deleteRocketNodeEvent = new EventEmitter<RocketNode>();

  private _nodes: RocketNode[] = [];
  private _unsubscribe$ = new Subject<void>();

  treeControl = new NestedTreeControl<RocketNode>(node => node.children);
  dataSource = new MatTreeNestedDataSource<RocketNode>();
  // REF: https://material.angular.io/components/tree/overview
  hasChild = (_: number, node: RocketNode) => node?.children?.length > 0;

  constructor(private matDialog: MatDialog) { }

  ngOnInit(): void {
    if (this.rocketNode)
      this._nodes.push(this.rocketNode);
    this.dataSource.data = this._nodes;
  }

  getNodePropertyKeys(properties: Record<string, number>): string[] {
    if (!properties) return [];
    return Object.keys(properties);
  }

  openDeleteNodeDialog(node: RocketNode): void {
    let dialogRef = this.matDialog.open(RocketConfirmDialogComponent, {
      data: node
    });

    dialogRef.afterClosed()
      .pipe(takeUntil(this._unsubscribe$))
      .subscribe(result => {
        // Send the node to delete to the parent component
        if (result)
          this.deleteRocketNodeEvent.emit(result);
      });
  }

  ngOnDestroy(): void {
    this._unsubscribe$.next();
    this._unsubscribe$.unsubscribe();
  }
}
