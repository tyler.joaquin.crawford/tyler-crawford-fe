import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { RocketNode } from 'src/app/shared/models/rocket-node';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing'
import { RocketsDisplayComponent } from './rockets-display.component';
import { MatDialogModule } from '@angular/material/dialog';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatTreeModule } from '@angular/material/tree';
import { MatTreeHarness } from '@angular/material/tree/testing';
import { HttpClient } from '@angular/common/http';
import { TestbedHarnessEnvironment } from '@angular/cdk/testing/testbed';
import { HarnessLoader } from '@angular/cdk/testing';
import { MatIconModule } from '@angular/material/icon';
import { TimeSincePipe } from 'src/app/shared/pipes/time-since.pipe';

describe('RocketsDisplayComponent', () => {
  let httpClient: HttpClient;
  let httpTestingController: HttpTestingController;
  let component: RocketsDisplayComponent;
  let fixture: ComponentFixture<RocketsDisplayComponent>;
  let loader: HarnessLoader;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [RocketsDisplayComponent, TimeSincePipe],
      imports: [
        HttpClientTestingModule,
        MatIconModule,
        MatToolbarModule,
        MatDialogModule,
        MatTreeModule],
    })
      .compileComponents();

    // ref: https://stackoverflow.com/questions/47236963/no-provider-for-httpclient
    httpClient = TestBed.inject(HttpClient);
    httpTestingController = TestBed.inject(HttpTestingController);

    fixture = TestBed.createComponent(RocketsDisplayComponent);
    component = fixture.componentInstance;

    const engine1 = new RocketNode();
    engine1.id = "22534-722358-789345-14143";
    engine1.name = "Engine1";
    engine1.properties = { "Mass": 11134.223 };

    const engine2 = new RocketNode();
    engine2.id = "15523-775123-141224-87845";
    engine2.name = "Engine2";
    engine2.properties = { "ISP": 9.441 };

    const stage1 = new RocketNode();
    stage1.id = "11223-442551-662421-09873";
    stage1.name = "Stage1";
    stage1.children = [engine1, engine2];

    const rocket = new RocketNode();
    rocket.id = "3243113-135135-13513513-135135";
    rocket.name = "Rocket1";
    rocket.children = [stage1];

    component.rocketNode = rocket;
    fixture.detectChanges();
    loader = TestbedHarnessEnvironment.loader(fixture);

  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should color label green when property value over 10', async () => {
    // ref: https://material.angular.io/components/tree/examples
    const tree = await loader.getHarness(MatTreeHarness);
    const treeDescendants = await tree.getNodes();
    expect(treeDescendants).toBeTruthy();

    // Expand all the nodes as they are initially collapsed
    for (let i = 0; i < treeDescendants.length; i++) {
      const desc = treeDescendants[i];
      await treeDescendants[0].expand();
    }

    // Testing the first TD with the applied class that colors the label green
    const td = fixture.debugElement
      .query(By.css(".rocket-tree-node-property-tbl .greater-than-ten"))
      .nativeElement;

    expect(getComputedStyle(td).color).toEqual("rgb(0, 128, 0)")
  })
});
