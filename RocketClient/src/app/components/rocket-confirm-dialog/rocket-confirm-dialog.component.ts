import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog'
import { RocketNode } from 'src/app/shared/models/rocket-node';

@Component({
  selector: 'app-rocket-confirm-dialog',
  templateUrl: './rocket-confirm-dialog.component.html',
  styleUrls: ['./rocket-confirm-dialog.component.css']
})
export class RocketConfirmDialogComponent {

  constructor(
    public dialogRef: MatDialogRef<RocketConfirmDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: RocketNode,
  ) { }

  onCancelClick(): void {
    this.dialogRef.close();
  }

  onConfirmClick(): void {
    this.dialogRef.close(this.data);
  }
}
